﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MinhaEmpresa.Agenda.Dominio.Entidades;
using MinhaEmpresa.Agenda.Dominio.Servicos;
using MinhaEmpresa.SharedKernel.Repositorio;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using MinhaEmpresa.Agenda.Api.Objects;
using MinhaEmpresa.Agenda.Dominio.ValueObjects;
using MinhaEmpresa.SharedKernel.Dominio.Repositorio;

namespace MinhaEmpresa.Agenda.Api.Controllers
{
    public class ClientController : ApiController
    {
        private ServiceClient service = null;
        private IRepositorioSessao sessao = null;

        public ClientController()
        {
            var helper = new RepositorioHelper();
            this.sessao = helper.AbrirSessao();
            this.service = new ServiceClient(sessao.GetRepositorio<Client>());
        }

        // GET api/<controller>
        public HttpResponseMessage Get()
        {
            var result = this.service.findArray();
            var code = HttpStatusCode.NoContent;
            var json = "";

            if (result.Any())
            {
                json = JsonConvert.SerializeObject(result);
                code = HttpStatusCode.OK;
            }

            return new HttpResponseMessage()
            {
                StatusCode = code,
                Content = new StringContent(json, System.Text.Encoding.UTF8, "application/json")
            };
        }

        // GET api/<controller>/5
        public HttpResponseMessage Get(int id)
        {
            var result = this.service.find(id);
            var code = HttpStatusCode.NoContent;
            var json = "";

            if (result != null) {
                json = JsonConvert.SerializeObject(result);
                code = HttpStatusCode.OK;
            }
            
            return new HttpResponseMessage()
            {
                StatusCode = code,
                Content = new StringContent(json, System.Text.Encoding.UTF8, "application/json")
            };
        }

        // POST api/<controller>
        public HttpResponseMessage Post([FromBody] string data)
        {
            HttpStatusCode code;
            try
            {
                ClientJSON json = new JavaScriptSerializer().Deserialize<ClientJSON>(data);

                Client client   = new Client();
                client.Name     = json.Name;
                client.Document = json.Document;

                DateTime date = DateTime.ParseExact(json.Card.ExpirationDate, "MM/yy", System.Globalization.CultureInfo.InvariantCulture);
                CreditCard card = new CreditCard(json.Card.Number, json.Card.CoBrandingPartners, date);
                var cards = new List<CreditCard> { card };
                client.createCards(cards);

                this.sessao.IniciaTransacao();
                this.service.create(client);
                this.sessao.ComitaTransacao();
                code = HttpStatusCode.OK;
            } catch (Exception e) {
                code = HttpStatusCode.InternalServerError;
            }

            return new HttpResponseMessage(code);
            
        }

        // PUT api/<controller>/5
        public HttpResponseMessage Put(int id, [FromBody] string data)
        {
            var client = this.service.find(id);
            if (client == null)
                return new HttpResponseMessage(HttpStatusCode.NotModified);

            ClientJSON json = new JavaScriptSerializer().Deserialize<ClientJSON>(data);

            client.Name     = json.Name;
            client.Document = json.Document;
            return new HttpResponseMessage(HttpStatusCode.NotModified);
        }

        // DELETE api/<controller>/5
        public HttpResponseMessage Delete(int id)
        {
            var result = this.service.find(id);
            if (result == null)
                return new HttpResponseMessage(HttpStatusCode.NotModified);

            try{
                this.sessao.IniciaTransacao();
                this.service.delete(id);
                this.sessao.ComitaTransacao();
                return new HttpResponseMessage(HttpStatusCode.OK);
            }catch (Exception e) {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
            return new HttpResponseMessage(HttpStatusCode.InternalServerError);
        }
    }
}