﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(MinhaEmpresa.Agenda.Api.AppStart.Startup))]

namespace MinhaEmpresa.Agenda.Api.AppStart
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ApiConfig.Configuration(app);
        }
    }
}
