﻿using Ninject;
using Owin;
using System.Reflection;
using System.Web.Http;

//[assembly: OwinStartup(typeof(MinhaEmpresa.Agenda.Api.AppStart.ApiConfig))]

namespace MinhaEmpresa.Agenda.Api.AppStart
{
    public class ApiConfig
    {
        public static void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            // Configuração das Rotas por Atributos
            config.MapHttpAttributeRoutes();

            // Configuração de Rotas Padrão do WebApi - Controllers individuais para cada Resource
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Removendo o formatter de xml para melhorar o uso pelo browser -- comentar essa linha se quiser suportar XML
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            // Configurando o WebApi
            app.UseWebApi(config);
            //app.UseNinjectMiddleware(CreateKernel).UseNinjectWebApi(config);


        }

        private static StandardKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            return kernel;
        }
    }
}
