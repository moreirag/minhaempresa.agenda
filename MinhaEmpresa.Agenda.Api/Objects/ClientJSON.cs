﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinhaEmpresa.Agenda.Api.Objects
{
    public class ClientJSON
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("document")]
        public string Document { get; set; }

        [JsonProperty("card")]
        public CardJSON Card { get; set; }
    }
}