﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MinhaEmpresa.Agenda.Api.Objects
{
    public class CardJSON
    {
        [JsonProperty("number")]
        public string Number { get; set; }

        [JsonProperty("cobrandingpartners")]
        public string CoBrandingPartners { get; set; }

        [JsonProperty("expirationdate")]
        public string ExpirationDate { get; set; }
    }
}