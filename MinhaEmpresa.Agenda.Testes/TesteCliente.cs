﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MinhaEmpresa.Agenda.Dominio.Entidades;
using MinhaEmpresa.Agenda.Dominio.ValueObjects;
using MinhaEmpresa.Agenda.Testes.Repositorio;
using System;
using System.Collections.Generic;


namespace MinhaEmpresa.Agenda.Testes
{
    [TestClass]
    public class TesteCliente : TesteRepositorioBase
    {
        public Client Client { get; set; }
        [TestInitialize]
        public void SetupContext()
        {
            // Preparação do teste
            Client = new Client();
            Client.Active           = false;
            Client.Email            = "gabrielemidiomoreira@gmail.com";
            Client.Document         = "41454592885";
            Client.Name             = "11Gabriel Emídi Moreira";
            Client.RegistrationDate = DateTime.Now;
        }

        [TestMethod]
        public void TestClienteCartao()
        {
            using (var sessao = Helper.AbrirSessao())
            {
                try
                {
                    DateTime date = DateTime.ParseExact("09/14", "MM/yy", System.Globalization.CultureInfo.InvariantCulture);
                    var card1 = new CreditCard("5308136911000057", "MASTERCARD", date);
                    var cards = new List<CreditCard> { card1 };
                    Client.createCards(cards);

                    sessao.IniciaTransacao();

                    var repo = sessao.GetRepositorio<Client>();
                    repo.Inclui(Client);
                    sessao.ComitaTransacao();

                }
                catch (Exception ex)
                {
                    sessao.RollBackTransacao();
                    Assert.Fail(ex.ToString());
                    throw;
                }
            }

        }
    }
}
