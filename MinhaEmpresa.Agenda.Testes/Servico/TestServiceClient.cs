﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MinhaEmpresa.Agenda.Dominio.Entidades;
using MinhaEmpresa.Agenda.Testes.Repositorio;
using MinhaEmpresa.Agenda.Dominio.Servicos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhaEmpresa.Agenda.Testes.Servico
{
    [TestClass]
    public class TestServiceClient : TesteRepositorioBase
    {

        [TestInitialize]
        public void Setup()
        {
            using (var sessao = Helper.AbrirSessao())
            {

                try
                {
                    Client client           = new Client();
                    client.Active           = false;
                    client.Email            = "gabrielemidiomoreira@gmail.com";
                    client.Document         = "41454592885";
                    client.Name             = "Gabriel Emídi Moreira";
                    client.RegistrationDate = DateTime.Now;
                    DateTime date = DateTime.ParseExact("09/14", "MM/yy", System.Globalization.CultureInfo.InvariantCulture);
                    client.addCard("5308136911000057", "MASTERCARD", date);

                    sessao.IniciaTransacao();

                    var repo = sessao.GetRepositorio<Client>();
                    repo.Inclui(client);

                    sessao.ComitaTransacao();
                }
                catch (Exception ex)
                {
                    sessao.RollBackTransacao();
                    Assert.Fail(ex.ToString());
                }
            }
        }

        [TestMethod]
        public void testFind()
        {
            using (var sessao = Helper.AbrirSessao())
            {
                try
                {
                    var repo = sessao.GetRepositorio<Client>();
                    ServiceClient service = new ServiceClient(repo);
                    sessao.IniciaTransacao();
                    var client = service.find(1);
                    Assert.IsNotNull(client);
                    sessao.ComitaTransacao();
                }
                catch (Exception ex)
                {
                    sessao.RollBackTransacao();
                    Assert.Fail(ex.ToString());
                }
            }
        }
    }
}
