﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MinhaEmpresa.SharedKernel.Dominio.Entidades;
using MinhaEmpresa.SharedKernel.Dominio.Repositorio;

namespace MinhaEmpresa.SharedKernel.Dominio.Servicos
{
    public class ServicoConsulta<T> where T : IEntidade
    {

        protected IRepositorioHelper RepositorioHelper { get; set; }

        public ServicoConsulta(IRepositorioHelper repositorioHelper)
        {
            RepositorioHelper = repositorioHelper;
        }
        
        public IEnumerable<T> Consulta()
        {
            using (var sessao = RepositorioHelper.AbrirSessao())
            {
                try
                {
                    sessao.IniciaTransacao();
                    var repo = sessao.GetRepositorioConsulta<T>();
                    var dados = repo.Consulta();
                    sessao.ComitaTransacao();
                    return dados.ToList();
                }
                catch (Exception)
                {
                    sessao.RollBackTransacao();
                    throw;
                }
            }
        }

        public IEnumerable<T> Consulta(int take)
        {
            using (var sessao = RepositorioHelper.AbrirSessao())
            {
                try
                {
                    sessao.IniciaTransacao();
                    var repo = sessao.GetRepositorioConsulta<T>();
                    var dados = repo.Consulta();
                    sessao.ComitaTransacao();
                    return dados.Take(take).ToList();
                }
                catch (Exception)
                {
                    sessao.RollBackTransacao();
                    throw;
                }
            }
        }

        public IEnumerable<T> Consulta(Expression<Func<T, bool>> @where)
        {
            using (var sessao = RepositorioHelper.AbrirSessao())
            {
                try
                {
                    sessao.IniciaTransacao();
                    var repo = sessao.GetRepositorioConsulta<T>();
                    var dados = repo.Consulta(@where);
                    sessao.ComitaTransacao();
                    return dados.ToList();
                }
                catch (Exception)
                {
                    sessao.RollBackTransacao();
                    throw;
                }
            }
        }
        
        public T Retorna(object id)
        {
            using (var sessao = RepositorioHelper.AbrirSessao())
            {
                try
                {
                    sessao.IniciaTransacao();
                    var repo = sessao.GetRepositorioConsulta<T>();
                    var entidade = repo.Retorna(id);
                    sessao.ComitaTransacao();
                    return entidade;
                }
                catch (Exception)
                {
                    sessao.RollBackTransacao();
                    throw;
                }
            }
        }

    }
}
