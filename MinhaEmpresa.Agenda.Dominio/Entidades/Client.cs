﻿using System;
using MinhaEmpresa.SharedKernel.Dominio.Entidades;
using MinhaEmpresa.Agenda.Dominio.ValueObjects;
using System.Collections.Generic;
using MinhaEmpresa.SharedKernel.Dominio.Agregacoes;

namespace MinhaEmpresa.Agenda.Dominio.Entidades
{
    public class Client : IAggregateRoot
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Document { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public DateTime RegistrationDate { get; set; }
        public CreditCard Card { get; set; }
        public IList<CreditCard> Cards { get; set; }

        //Comportamento que depois será abstraido com Services
        public bool ClienteVip(Client client)
        {
            return client.Active && DateTime.Now.Year - client.RegistrationDate.Year >= 3;
        }

        public void createCards(IList<CreditCard> cartoes)
        {
            Cards = new List<CreditCard>();
            foreach (var card in cartoes) {
                Cards.Add(card);
            }
        }

        public void addCard(string number, string cobranding, DateTime expiration_date)
        {
            this.Card = new CreditCard(number, cobranding, expiration_date);
        }
        
        public Client()
        {

        }

    }
}