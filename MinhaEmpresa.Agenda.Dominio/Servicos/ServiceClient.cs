﻿using MinhaEmpresa.Agenda.Dominio.Entidades;
using MinhaEmpresa.SharedKernel.Dominio.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhaEmpresa.Agenda.Dominio.Servicos
{
    public class ServiceClient
    {
        private readonly IRepositorio<Client> _repositorioClient;
        protected IRepositorioHelper Helper;

        public ServiceClient(IRepositorio<Client> repositorioClient)
        {
            _repositorioClient = repositorioClient;
        }

        public Boolean create(Client client)
        {
            client.Active = true;
            client.RegistrationDate = DateTime.Now;
            _repositorioClient.Inclui(client);
            return true;
        }

        public Client find(int id)
        {
            var client = _repositorioClient.Consulta(cli => cli.Id.Equals(id)).FirstOrDefault();
            return client;
        }

        public IEnumerable<Client> findArray()
        {
            var client = _repositorioClient.Consulta();
            return client;
        }

        public Boolean delete(int id)
        {
            try {
                _repositorioClient.Exclui(this.find(id));
                return true;
            }
            catch (Exception e) {
                return false;
            }
            return false;
        }
    }
}
