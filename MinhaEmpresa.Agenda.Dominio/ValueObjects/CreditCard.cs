﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhaEmpresa.Agenda.Dominio.ValueObjects
{
    public class CreditCard
    {
        public string Number { get; private set; }
        public string CoBrandingPartners { get; private set; }
        public string MaskNumber { get; private set; }
        public DateTime ExpirationDate { get; private set; }

        private const String FLAG_VISA       = "VISA";
        private const String FLAG_MASTERCARD = "MASTERCARD";
        private const String FLAG_DINNERCLUB = "DINNER CLUB";

        private int[] VISA        = {4};
        private int[] MASTERCARD  = {51, 52, 53, 54, 55};
        private int[] DINNER_CLUB = { 36, 38 };

        public CreditCard(string number, string cobranding, DateTime expiration_date)
        {
            this.Number             = number;
            this.CoBrandingPartners = cobranding;
            this.MaskNumber         = "*****";
            this.ExpirationDate     = expiration_date;
        }

        public CreditCard(){ }

        public Boolean VerificarCartao(CreditCard Card)
        {
            if (Card.Number.Length != 16)
                return false;

            if (DateTime.Compare(Card.ExpirationDate, DateTime.Now) < 0)
                return false;

            if (!this.ValidNumber(Card.Number))
                return false;

            if (!this.ValidCoBranding(Card))
                return false;

            return true;
        }

        private Boolean ValidNumber(String number)
        {
            int soma = 0;
            for (int i = 1; i <= number.Length; i++ )
            {
                if (i % 2 == 1) {
                    int resultado = Int32.Parse(number[i-1].ToString()) * 2;
                    soma += (resultado > 9) ? resultado - 9 : resultado;
                } else {
                    soma += Int32.Parse(number[i-1].ToString());
                }
            }

            if (((soma % 10) == 0) && (soma <= 150))
                return true;
            return false;
        }

        private Boolean ValidCoBranding(CreditCard Card)
        {
            switch (Card.CoBrandingPartners) { 
            
                case FLAG_VISA:
                    if (Array.IndexOf(VISA, Int16.Parse(Card.Number.Substring(0, 1))) >= 0)
                        return true;
                    break;
                case FLAG_MASTERCARD:
                    if (Array.IndexOf(MASTERCARD, Int16.Parse(Card.Number.Substring(0, 2))) >= 0)
                        return true;
                    break;
                case FLAG_DINNERCLUB:
                    if (Array.IndexOf(DINNER_CLUB, Int16.Parse(Card.Number.Substring(0, 2))) >= 0)
                        return true;
                    break;
            }
            return false;
        }
    }
}
